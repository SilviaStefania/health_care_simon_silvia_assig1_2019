package com.example.springdemo.services;


import com.example.springdemo.entities.AppUser;
import com.example.springdemo.entities.MedicalPlan;
import com.example.springdemo.entities.Roles;
import com.example.springdemo.repositories.RepositoryFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AppUserService {

    private final RepositoryFactory repositoryFactory;

    public AppUser save(AppUser appUser) {
        return repositoryFactory.createAllUsersRepository().save(appUser);
    }

    public void removeAppUser(AppUser appUser) {
        repositoryFactory.createAllUsersRepository().delete(appUser);
    }

    public Optional<AppUser> findById(int id) {
        return repositoryFactory.createAllUsersRepository().findById(id);
    }

    public List<AppUser> findAllUsers() {
        return repositoryFactory.createAllUsersRepository().findAll();
    }

    public List<AppUser> findAllPatientsOfCaregiver(AppUser caregiver) {
        List<AppUser> singleCaregiver = new ArrayList<>();
        singleCaregiver.add(caregiver);
        return repositoryFactory.createAllUsersRepository().findAppUsersByCaregivers(singleCaregiver);
    }

    public List<AppUser> findAllPatients(){
        return repositoryFactory.createAllUsersRepository().findAppUsersByRole(Roles.PATIENT);
    }

    public List<AppUser> findAllCaregivers(){
        return repositoryFactory.createAllUsersRepository().findAppUsersByRole(Roles.CAREGIVER);
    }

    public Optional<AppUser> findByUsername(String username){
        return repositoryFactory.createAllUsersRepository().findByUsername(username);
    }

}
