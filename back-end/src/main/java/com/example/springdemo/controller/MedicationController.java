package com.example.springdemo.controller;


import com.example.springdemo.dto.CaregiverSeenByDoctor;
import com.example.springdemo.dto.MedicationSeenByDoctor;
import com.example.springdemo.entities.AppUser;
import com.example.springdemo.entities.Medication;
import com.example.springdemo.entities.SessionId;
import com.example.springdemo.services.MedicationService;
import lombok.RequiredArgsConstructor;
import org.jboss.logging.annotations.Pos;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@org.springframework.web.bind.annotation.RestController
@CrossOrigin
@RequiredArgsConstructor
public class MedicationController {

    private final MedicationService medicationService;

    @GetMapping(value = "/medications")
    public List<MedicationSeenByDoctor> findAllMedications() {
        List<Medication> allMeds = medicationService.findAllMedication();
        List<MedicationSeenByDoctor> medicationSeenByDoctors = new ArrayList<>();
        for (Medication medication : allMeds) {
            medicationSeenByDoctors.add(new MedicationSeenByDoctor(medication));
        }
        return medicationSeenByDoctors;
    }

    @PostMapping(value = "/createMedication")
    public void addMedication(@RequestBody Medication medication) {
        medicationService.save(medication);
    }

    @DeleteMapping(value = "/deleteMedication")
    public String deleteMedication(@RequestBody SessionId requiredId) {
        String confirmation = "No such medication";
        Integer id = Integer.parseInt(requiredId.getIdToDelete());
        if (medicationService.findById(id).isPresent()) {
            confirmation = "Medication was deleted.";
            medicationService.removeMedication(medicationService.findById(id).get());
        }
        return confirmation;
    }

    @PostMapping(value = "/updateMedication")
    public String updateMedication(@RequestBody SessionId session) {
        String confirmation = "There is no medication to update";
        Integer id = Integer.parseInt(session.getIdToUpdate());
        if (medicationService.findById(id).isPresent()) {
                confirmation = "Update done.";
                Medication medicationToUpdate = medicationService.findById(id).get();
                if (!session.getMedName().equals("")) {
                    medicationToUpdate.setMedName(session.getMedName());
                }
                if (!session.getDosage().equals("")) {
                    medicationToUpdate.setDosage(session.getDosage());
                }
                if (!session.getSideEffects().equals("")) {
                    medicationToUpdate.setSideEffects(session.getSideEffects());
                }

            medicationService.save(medicationToUpdate);
            }
        return confirmation;

    }

}
