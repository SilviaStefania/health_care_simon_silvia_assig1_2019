package com.example.springdemo.controller;


import com.example.springdemo.entities.AppUser;
import com.example.springdemo.entities.SessionId;
import com.example.springdemo.services.AppUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequiredArgsConstructor
public class LoginController {

    private final AppUserService appUserService;

    @PostMapping(value = "/")
    public SessionId login(@RequestBody SessionId credentials){
        SessionId sessionId = new SessionId();

        String username = credentials.getUsername();
        String password = credentials.getPassword();

        if(appUserService.findByUsername(username).isPresent()){
            AppUser appUser = appUserService.findByUsername(username).get();
            if(appUser.getPassword().equals(password)){
                sessionId.setRole( appUser.getRole());
                sessionId.setSessionId(appUser.getAppUserId().toString());
            }

        }
       return sessionId;
    }


}
