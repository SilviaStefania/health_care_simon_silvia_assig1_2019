package com.example.springdemo.repositories;

import com.example.springdemo.entities.Medication;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;

public interface MedicationRepository extends Repository<Medication, Integer> {

    Medication save(Medication medication);
    List<Medication> findAll();
    Optional<Medication> findById(int id);
    void delete(Medication medication);


}
