package com.example.springdemo.repositories;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RepositoryFactoryImplementation implements RepositoryFactory {

    private final AppUserRepository appUserRepository;
    private final MedicalPlanRepository medicalPlanRepository;
    private final MedicationRepository medicationRepository;

    @Override
    public AppUserRepository createAllUsersRepository() {
        return appUserRepository;
    }

    @Override
    public MedicationRepository createMedicationRepository() {
        return medicationRepository;
    }

    @Override
    public MedicalPlanRepository createMedicalPlanRepository() {
        return medicalPlanRepository;
    }
}
