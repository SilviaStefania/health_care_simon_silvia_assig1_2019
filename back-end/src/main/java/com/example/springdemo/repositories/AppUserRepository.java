package com.example.springdemo.repositories;

import com.example.springdemo.entities.AppUser;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;

public interface AppUserRepository extends Repository<AppUser, Integer> {

    AppUser save(AppUser appUser);
    List<AppUser> findAll();
    Optional<AppUser> findById(int id);
    void delete(AppUser appUser);

    List<AppUser> findAppUsersByCaregivers(List<AppUser> caregivers);

    Optional<AppUser> findByUsername(String username);

    List<AppUser> findAppUsersByRole(String role);


}
