package com.example.springdemo.repositories;

import com.example.springdemo.entities.AppUser;
import com.example.springdemo.entities.MedicalPlan;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;

public interface MedicalPlanRepository extends Repository<MedicalPlan, Integer> {

    MedicalPlan save(MedicalPlan medicalPlan);
    List<MedicalPlan> findAll();
    Optional<MedicalPlan> findById(int id);
    void delete(MedicalPlan medicalPlan);

    List<MedicalPlan> findMedicalPlansByPatient(AppUser appUser);

}
