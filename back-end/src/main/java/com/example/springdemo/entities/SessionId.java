package com.example.springdemo.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data@AllArgsConstructor
@NoArgsConstructor
public class SessionId {
    private String sessionId;
    private String role;
    private String idToDelete;
    private String idToUpdate;

    private String name; //patient
    private String address;
    private String gender;
    private String bday;

    private String medName; //medication
    private String dosage;
    private String sideEffects;

    private String username;
    private String password;

    private String patientId; //fort he medical plan
    private String doctorId;
    private List<Integer> meds;
    private String startMed;
    private String stopMed;
    private String timePeriod;






}
