package com.example.springdemo.entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "app_user")
public class AppUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer appUserId;
    private String username;
    private String password;
    private String role;
    private String name;
    private String address;
    private String gender;
    private String bday;


    @OneToMany(mappedBy = "patient")
    private List<MedicalPlan> medicalPlans;

    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(name = "caregiver_patient",
            joinColumns = @JoinColumn(name = "patient_id"),
            inverseJoinColumns = @JoinColumn(name = "caregiver_id")
    )
    private List<AppUser> caregivers = new ArrayList<>();

    @ManyToMany(mappedBy = "caregivers")
    private List<AppUser> patients;


    public AppUser(Integer id, String username, String password, String role, String name,
                    String bday, String gender, String address, List<AppUser> patients) {
        this.appUserId = id;
        this.username = username;
        this.password = password;
        this.role = role;
        this.name = name;
        this.bday = bday;
        this.gender = gender;
        this.address = address;
        this.patients = patients;
        this.patients.forEach(p -> p.getCaregivers().add(this));
    }

    public AppUser(Integer id, String username, String password, String role, String name,
                   String bday, String gender, String address) {
        this.appUserId = id;
        this.username = username;
        this.password = password;
        this.role = role;
        this.name = name;
        this.bday = bday;
        this.gender = gender;
        this.address = address;
    }



    public String toString(){
        return this.name + " " + this.role ;
    }

}
