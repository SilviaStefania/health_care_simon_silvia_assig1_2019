 package com.example.springdemo.dto;

import com.example.springdemo.entities.AppUser;
import com.example.springdemo.entities.MedicalPlan;
import com.example.springdemo.entities.Medication;
import com.fasterxml.jackson.databind.deser.std.MapEntryDeserializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public class MedicationSeenByDoctor {

        private Integer medId;
        private String medName;
        private String dosage;
        private String sideEffects;
        //  List<MedicalPlan> medicalPlans;

        public MedicationSeenByDoctor(Medication medication){
            this.medId = medication.getMedId();
            this.medName = medication.getMedName();
            this.dosage = medication.getDosage();
            this.sideEffects = medication.getSideEffects();
        }


    }

