export default interface Medication{
    medId?: number,
    medName: string,
    dosage: string,
    sideEffects: string
};