import {makeStyles, Paper, Table} from "@material-ui/core";
import React, {useState} from "react";
import SimpleTable from "./Table";
import PatientsTable from "./tables/PatientsTable";
import AddPatientForm from "./forms/AddPatientForm"
import AddCaregiverForm from "./forms/caregiverForm/AddCaregiverForm"
import DeleteCaregiverForm from "./forms/caregiverForm/DeleteCaregiverForm"
import UpdateCaregiverForm from "./forms/caregiverForm/UpdateCaregiverForm"
import DeletePatientForm from "./forms/DeletePatientForm"
import UpdatePatientForm from "./forms/UpdatePatientForm"
import AddMedicationForm from "./forms/medicationsForm/AddMedicationForm"
import DeleteMedicationForm from "./forms/medicationsForm/DeleteMedicationForm"
import UpdateMedicationForm from "./forms/medicationsForm/UpdateMedicationForm"
import Button from "@material-ui/core/Button";
import axios from "axios";
import CaregiverTable from "./tables/CaregiversTable";
import MedicationsTable from "./tables/MedicationsTable";


const doctorStyle = makeStyles(theme => ({
    '@global': {
        body: {
            backgroundColor: theme.palette.common.white,
        },
    },
    paper: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3),

    },
    h1: {
        marginLeft: theme.spacing(7),
    },
    h3: {
        marginLeft: theme.spacing(4),
    },
    button: {
        margin: "20px 20px",
    },

}));


export default function () {
    const classes = doctorStyle();
    const [tableType, setTableType] = useState(0);

    function getAllPatientsButton(tableType: number) {
        setTableType(tableType);
    };

    function getAllCaregiversButton(tableType: number) {
        setTableType(tableType);
    }

    function getAllMedicationsButton(tableType: number) {
        setTableType(tableType);
    }

    function logout(){
        window.location.assign("/");
        localStorage.clear();
    }

    return (

        <div className={classes.paper}>

            <form>

                <h1 className={classes.h1}>

                    <Button className={classes.button} onClick={() => logout()}
                            type="button"
                            variant="contained"
                    >
                        Logout
                    </Button>

                    What`s up, doc?
                </h1>
            </form>

            <Paper>

                <Button className={classes.button} onClick={() => getAllPatientsButton(1)}
                        type="button"
                        variant="contained"
                        color="primary"
                    /*  className={classes.submit}*/
                >
                    Patients
                </Button>

                <Button className={classes.button} onClick={() => getAllCaregiversButton(2)}
                        type="button"
                        variant="contained"
                        color="primary"
                >
                    Caregivers
                </Button>

                <Button className={classes.button}  onClick={() => getAllMedicationsButton(3)}
                        type="button"
                        variant="contained"
                        color="primary"
                    /*  className={classes.submit}*/
                >
                    All Medications
                </Button>


                {tableType == 1 ? <PatientsTable/> : (tableType == 2? <CaregiverTable/> : <MedicationsTable/>)}
                {tableType == 1 ? <AddPatientForm/> : (tableType == 2?  <AddCaregiverForm/> : <AddMedicationForm/>)}
                {tableType == 1 ? <DeletePatientForm/> : (tableType == 2 ? <DeleteCaregiverForm/>: <DeleteMedicationForm/>)}


                <h3 className={classes.h3}>
                    Enter the id  you want the update and the new value in the corresponding field.
                </h3>

              {tableType == 1 ? <UpdatePatientForm/> : ( tableType == 2? <UpdateCaregiverForm/> : <UpdateMedicationForm/> )}



            </Paper>
        </div>


    );
}