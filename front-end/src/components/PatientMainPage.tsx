import {makeStyles, Paper, Table} from "@material-ui/core";
import React, {useState} from "react";
import Button from "@material-ui/core/Button";
import axios from "axios";
import MedicalPlansSeenByPatients from "./tables/MedicalPlansSeenByPatients";


const patientStyle = makeStyles(theme => ({
    '@global': {
        body: {
            backgroundColor: theme.palette.common.white,
        },
    },
    paper: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3),

    },
    h1: {
        marginLeft: theme.spacing(7),
    },
    h3: {
        marginLeft: theme.spacing(4),
    },
    button: {
        margin: "20px 20px",
    },

}));


export default function () {
    const classes = patientStyle();

    function logout(){
        window.location.assign("/");
        localStorage.clear();
    }

    return (

        <div className={classes.paper}>

            <form>

                <h1 className={classes.h1}>

                    <Button className={classes.button} onClick={() => logout()}
                            type="button"
                            variant="contained"
                    >
                        Logout
                    </Button>

                    Hello!
                </h1>
            </form>

            <Paper>

                <MedicalPlansSeenByPatients/>


            </Paper>
        </div>


    );
}