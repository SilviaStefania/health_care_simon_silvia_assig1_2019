import {makeStyles, Paper, Table} from "@material-ui/core";
import React, {useState} from "react";
import SimpleTable from "./Table";
import PatientsTable from "./tables/PatientsTable";
import AddPatientForm from "./forms/AddPatientForm"
import AddCaregiverForm from "./forms/caregiverForm/AddCaregiverForm"
import DeleteCaregiverForm from "./forms/caregiverForm/DeleteCaregiverForm"
import UpdateCaregiverForm from "./forms/caregiverForm/UpdateCaregiverForm"
import DeletePatientForm from "./forms/DeletePatientForm"
import UpdatePatientForm from "./forms/UpdatePatientForm"
import AddMedicationForm from "./forms/medicationsForm/AddMedicationForm"
import DeleteMedicationForm from "./forms/medicationsForm/DeleteMedicationForm"
import UpdateMedicationForm from "./forms/medicationsForm/UpdateMedicationForm"
import Button from "@material-ui/core/Button";
import axios from "axios";
import CaregiverTable from "./tables/CaregiversTable";
import MedicationsTable from "./tables/MedicationsTable";


const caregiverStyle = makeStyles(theme => ({
    '@global': {
        body: {
            backgroundColor: theme.palette.common.white,
        },
    },
    paper: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3),

    },
    h1: {
        marginLeft: theme.spacing(7),
    },
    h3: {
        marginLeft: theme.spacing(4),
    },
    button: {
        margin: "20px 20px",
    },

}));


export default function () {
    const classes = caregiverStyle();

    function logout(){
        window.location.assign("/");
        localStorage.clear();
    }

    return (

        <div className={classes.paper}>

            <form>

                <h1 className={classes.h1}>

                    <Button className={classes.button} onClick={() => logout()}
                            type="button"
                            variant="contained"
                    >
                        Logout
                    </Button>

                    These are your patients. Take good care of them :)
                </h1>
            </form>

            <Paper>

                <PatientsTable/>


            </Paper>
        </div>


    );
}