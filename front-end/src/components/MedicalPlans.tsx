import {makeStyles, Paper, Table} from "@material-ui/core";
import React, {useState} from "react";
import SimpleTable from "./Table";
import PatientsTable from "./tables/PatientsTable";
import Button from "@material-ui/core/Button";
import axios from "axios";


const doctorStyle = makeStyles(theme => ({
    '@global': {
        body: {
            backgroundColor: theme.palette.common.white,
        },
    },
    paper: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3),

    },
    h1: {
        marginLeft: theme.spacing(7),
    },
    button: {
        margin: "20px 20px",
    },

}));


export default function () {
    const classes = doctorStyle();
    const [tableType, setTableType] = useState(0);
    function getAllPatientsButton(tableType:number) {
        setTableType(tableType);
    };


    return (

        <div className={classes.paper}>

            <div>
                <h1 className={classes.h1}>
                    [Pacients name] medical plans
                </h1>
            </div>

            <Paper>

                <Button className={classes.button} onClick={() => window.location.assign("/doctor")}
                        type="button"
                        variant="contained"
                        color="primary"
                    /*  className={classes.submit}*/
                >
                    Back
                </Button>





            </Paper>
        </div>


    );
}