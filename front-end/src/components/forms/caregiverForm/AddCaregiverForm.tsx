import React, {useState} from 'react';
import {makeStyles, Theme, createStyles} from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/Button";
import Caregiver from "../../../types/Caregiver";
import axios from "axios"


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        textField: {
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
        },
        dense: {
            marginTop: theme.spacing(2),
        },
        menu: {
            width: 200,
        },
        button: {
            margin: "20px 20px",
        },
    }),
);


export default function OutlinedTextFields() {
    const classes = useStyles();


    function createCaregiver() {
        const caregiver: Caregiver = {
            name: name,
            address: address,
            gender: gender,
            bday: bday
        }

        axios.post("http://localhost:8080/createCaregiver", caregiver).then(response => {
                alert("Caregiver added. ");
                window.location.reload();
            }
        );


        // @ts-ignore
        document.getElementById("outlined-name").value = "";
        // @ts-ignore
        document.getElementById("outlined-address").value = "";
        // @ts-ignore
        document.getElementById("outlined-gender").value = "";
        // @ts-ignore
        document.getElementById("outlined-bday").value = "";

    }

    const [name, setName] = useState("");
    const [address, setAddress] = useState("");
    const [gender, setGender] = useState("");
    const [bday, setBday] = useState("");

    return (
        <form className={classes.container} noValidate autoComplete="off">
            <TextField
                id="outlined-name"
                label="Name"
                className={classes.textField}
                onChange={(e) => setName(e.target.value)}
                margin="normal"
                variant="outlined"
            />
            <TextField
                id="outlined-address"
                label="Address"
                className={classes.textField}
                onChange={(e) => setAddress(e.target.value)}
                margin="normal"
                variant="outlined"
            />
            <TextField

                id="outlined-gender"
                label="Gender"
                onChange={(e) => setGender(e.target.value)}
                className={classes.textField}
                margin="normal"
                variant="outlined"
            />
            <TextField

                id="outlined-bday"
                label="Birth Day"
                onChange={(e) => setBday(e.target.value)}
                className={classes.textField}
                margin="normal"
                variant="outlined"
            />

            <Button className={classes.button} onClick={() => createCaregiver()}
                    type="button"
                    variant="contained"
                    color="secondary"
                /*  className={classes.submit}*/
            >
                Add Caregiver
            </Button>

        </form>
    );
}