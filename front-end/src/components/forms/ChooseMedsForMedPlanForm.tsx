import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import axios from "axios";
import Medication from "../../types/Medication";
import CreateMedicalPlanByDoctor from "./CreateMedicalPlanByDoctor";
import {Button} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import MedicalPlan from "../../types/MedicalPlan";
import Patient from "../../types/Patient";

const useStyles = makeStyles({
    root: {
        width: '100%',
        overflowX: 'auto',
    },
    table: {
        minWidth: 650,
    },
    button: {
        backgroundColor: "indigo",
        color: "yellow",
    },
});

const medsForPlan: number[] = [];

export default function MedicationsTable(patientMedPlan: Patient) {
    const classes = useStyles();
    const [rows, setRows] = useState<Medication[]>([]);


    const [meds, setMeds] = useState<number[]>([]);
    useEffect(() => {
        axios.get("http://localhost:8080/medications").then(response => {
            console.log(response.data);
            setRows(response.data);
        });
    }, []);





    function addMedsForPlan(medicationToAdd: number | undefined) {
        // @ts-ignore
        /* document.getElementById("butonConfirmare").innerHTML= "dsfdsf";*/

        if (medicationToAdd != undefined) {
            debugger
            medsForPlan[medsForPlan.length] = medicationToAdd;
            setMeds(medsForPlan);
        }
    }

    function addMedicalPlan() {
        let patientid: number = 0;
        let docId: number = 0;

        if (patientMedPlan.id != undefined) {
            patientid = patientMedPlan.id;
        }
        if (localStorage.getItem("sessionId") != null) {
            docId = parseInt(localStorage.getItem("sessionId") as string);
        }

        const medPlan: MedicalPlan = {

            patientId: patientid,
            doctorId: docId,
            meds: meds,
            startMed: startMed,
            stopMed: stopMed,
            timePeriod: timePeriod
        }

        axios.post("http://localhost:8080/addMedicalPlan", {
            patientId: patientid,
            doctorId: docId,
            meds: medsForPlan,
            startMed: startMed,
            stopMed: stopMed,
            timePeriod: timePeriod
        }).then(response => {
            debugger
            alert(response.data)
        });
    }

    const [startMed, setStartMed] = useState("");
    const [stopMed, setStopMed] = useState("");
    const [timePeriod, setTimePeriod] = useState("");


    return (
        <div>
            <Paper className={classes.root}>
                <Table className={classes.table} stickyHeader aria-label="sticky table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Medication ID</TableCell>
                            <TableCell align="right">Medication Name</TableCell>
                            <TableCell align="right">Dosage</TableCell>
                            <TableCell align="right">Side Effects</TableCell>
                            <TableCell align="right"> </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.map(row => (
                            <TableRow key={row.medId}>
                                <TableCell component="th" scope="row">
                                    {row.medId}
                                </TableCell>
                                <TableCell align="right">{row.medName}</TableCell>
                                <TableCell align="right">{row.dosage}</TableCell>
                                <TableCell align="right">{row.sideEffects}</TableCell>
                                <TableCell align="right">
                                    <Button id={"butonConfirmare"}
                                            className={classes.button}
                                            onClick={() => addMedsForPlan(row.medId)}>
                                        Add
                                    </Button>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>

                <TextField
                    id="outlined-start-intake"
                    label="Start Intake interval"
                    margin="normal"
                    variant="outlined"
                    onChange={(e) => setStartMed(e.target.value)}
                />

                <TextField
                    id="outlined-stop-intake"
                    label="Stop Intake interval"
                    margin="normal"
                    variant="outlined"
                    onChange={(e) => setStopMed(e.target.value)}
                />

                <TextField
                    id="outlined-period"
                    label="Period of treatment"
                    margin="normal"
                    variant="outlined"
                    onChange={(e) => setTimePeriod(e.target.value)}
                />


            </Paper>

            <Button
                className={classes.button}
                onClick={() => addMedicalPlan()}
            >
                OK, Add to medical Plan
            </Button>

        </div>
    );
}