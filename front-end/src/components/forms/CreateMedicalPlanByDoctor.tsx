import React from 'react';
import {makeStyles, Theme, createStyles} from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import {Button} from "@material-ui/core";
import PatientsTable from "../tables/PatientsTable";
import RowId from "../../types/RowId";
import Patient from "../../types/Patient";
import ChooseMedsForMedPlanForm from "../forms/ChooseMedsForMedPlanForm"

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        modal: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
        },
        paper: {
            backgroundColor: theme.palette.background.paper,
            border: '2px solid #000',
            boxShadow: theme.shadows[5],
            padding: theme.spacing(2, 4, 3),
        },
        button: {
            backgroundColor: "indigo",
            color: "yellow",
        },
    }),
);

export default function TransitionsModal(patientToAddMedicalPlan: Patient) {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);

    function handleOpen(rowId: number | undefined) {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div>
            <Button className={classes.button} onClick={() => handleOpen(patientToAddMedicalPlan.id)}>
                Add Medical Plan
            </Button>

            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className={classes.paper}>
                        <h2> Add Medical Plan to patient with id {patientToAddMedicalPlan.id}
                        </h2>

                        <ChooseMedsForMedPlanForm  id={patientToAddMedicalPlan.id} address={""} bday={""} gender={""} name={""}/>




                    </div>
                </Fade>


            </Modal>
        </div>
    );
}