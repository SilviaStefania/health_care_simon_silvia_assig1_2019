import React, {useState} from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/Button";
import axios from "axios"

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        textField: {
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
        },
        dense: {
            marginTop: theme.spacing(2),
        },
        menu: {
            width: 200,
        },
        button: {
            margin: "20px 20px",
        },
    }),
);


export default function OutlinedTextFields() {
    const classes = useStyles();

    function deletePatient(){
       /* const patient : Patient = {
            name : name,
            address : address,
            gender : gender,
            bday : bday
        }*/

        axios.delete("http://localhost:8080/deletePatient", {data:{idToDelete:patientId}}).then(response =>{
            alert(response.data);
            window.location.reload();
            }
        );

        // @ts-ignore
        document.getElementById("outlined-patientId").value = "";


    }

    const [patientId, setPatientId] = useState("");

    return (
        <form className={classes.container} noValidate autoComplete="off">
            <TextField
                id="outlined-patientId"
                label="Patient Id"
                className={classes.textField}
                onChange={(e) => setPatientId(e.target.value)}
                margin="normal"
                variant="outlined"
            />

            <Button className={classes.button} onClick={ () => deletePatient()}
                    type="button"
                    variant="contained"
                    color="secondary"
            >
                Delete Patient
            </Button>

        </form>
    );
}