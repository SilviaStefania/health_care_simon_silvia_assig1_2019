import React, {useState} from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/Button";
import axios from "axios"

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        textField: {
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
        },
        dense: {
            marginTop: theme.spacing(2),
        },
        menu: {
            width: 200,
        },
        button: {
            margin: "20px 20px",
        },
    }),
);


export default function OutlinedTextFields() {
    const classes = useStyles();

    function deleteMedication(){

        axios.delete("http://localhost:8080/deleteMedication", {data:{idToDelete:medicationId}}).then(response =>{
                alert(response.data);
                window.location.reload();
            }
        );

        // @ts-ignore
        document.getElementById("outlined-patientId").value = "";


    }

    const [medicationId, setMedicationId] = useState("");

    return (
        <form className={classes.container} noValidate autoComplete="off">
            <TextField
                id="outlined-patientId"
                label="Medication Id"
                className={classes.textField}
                onChange={(e) => setMedicationId(e.target.value)}
                margin="normal"
                variant="outlined"
            />

            <Button className={classes.button} onClick={ () => deleteMedication()}
                    type="button"
                    variant="contained"
                    color="secondary"
            >
                Delete Medication
            </Button>

        </form>
    );
}