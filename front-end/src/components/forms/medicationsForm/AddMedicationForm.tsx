import React, {useState} from 'react';
import {makeStyles, Theme, createStyles} from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/Button";
import axios from "axios"
import Medication from "../../../types/Medication";


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        textField: {
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
        },
        dense: {
            marginTop: theme.spacing(2),
        },
        menu: {
            width: 200,
        },
        button: {
            margin: "20px 20px",
        },
    }),
);


export default function OutlinedTextFields() {
    const classes = useStyles();


    function createMedication() {
        const medication: Medication = {
            medName: medName,
            dosage: dosage,
            sideEffects: sideEffects
        }

        axios.post("http://localhost:8080/createMedication", medication).then(response => {
                alert("Medication added. ");
                window.location.reload();
            }
        );


    }

    const [medName, setMedName] = useState("");
    const [dosage, setDosage] = useState("");
    const [sideEffects, setSideEffects] = useState("");

    return (
        <form className={classes.container} noValidate autoComplete="off">
            <TextField
                id="outlined-name"
                label="Medication Name"
                className={classes.textField}
                onChange={(e) => setMedName(e.target.value)}
                margin="normal"
                variant="outlined"
            />
            <TextField
                id="outlined-address"
                label="Dosage"
                className={classes.textField}
                onChange={(e) => setDosage(e.target.value)}
                margin="normal"
                variant="outlined"
            />
            <TextField

                id="outlined-gender"
                label="Side Effects"
                onChange={(e) => setSideEffects(e.target.value)}
                className={classes.textField}
                margin="normal"
                variant="outlined"
            />

            <Button className={classes.button} onClick={() => createMedication()}
                    type="button"
                    variant="contained"
                    color="secondary"
                /*  className={classes.submit}*/
            >
                Add Medication
            </Button>

        </form>
    );
}