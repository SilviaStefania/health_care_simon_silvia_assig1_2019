import React, {useState} from 'react';
import clsx from 'clsx';
import {makeStyles, Theme, createStyles} from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/Button";
import axios from "axios"


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        textField: {
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
        },
        dense: {
            marginTop: theme.spacing(2),
        },
        menu: {
            width: 200,
        },
        button: {
            margin: "20px 20px",
        },
    }),
);


export default function OutlinedTextFields() {
    const classes = useStyles();


    function updateMedication() {


        axios.post("http://localhost:8080/updateMedication", {idToUpdate:medicationId, medName, dosage, sideEffects}).then(response => {
                alert(response.data);
                window.location.reload();
            }
        );

    }

    const [medicationId, setMedicationId] = useState("");
    const [medName, setMedName] = useState("");
    const [dosage, setDosage] = useState("");
    const [sideEffects, setSideEffect] = useState("");

    return (
        <form className={classes.container} noValidate autoComplete="off">


            <TextField
                id="outlined-medicationId"
                label="Medication Id"
                className={classes.textField}
                onChange={(e) => setMedicationId(e.target.value)}
                margin="normal"
                variant="outlined"
            />


            <TextField
                id="outlined-name"
                label="Name"
                className={classes.textField}
                onChange={(e) => setMedName(e.target.value)}
                margin="normal"
                variant="outlined"
            />
            <TextField
                id="outlined-address"
                label="Dosage"
                className={classes.textField}
                onChange={(e) => setDosage(e.target.value)}
                margin="normal"
                variant="outlined"
            />
            <TextField

                id="outlined-gender"
                label="Side Effect"
                onChange={(e) => setSideEffect(e.target.value)}
                className={classes.textField}
                margin="normal"
                variant="outlined"
            />


            <Button className={classes.button} onClick={() => updateMedication()}
                    type="button"
                    variant="contained"
                    color="secondary"
            >
                Update Medication
            </Button>

        </form>
    );
}