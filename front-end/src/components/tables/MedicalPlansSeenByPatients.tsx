import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Caregiver from "../../types/Caregiver";
import axios from "axios";
import MedicalPlan from "../../types/MedicalPlan";

const useStyles = makeStyles({
    root: {
        width: '100%',
        overflowX: 'auto',
    },
    table: {
        minWidth: 650,
    },
    button: {
        margin: "20px 20px",
        backgroundColor: "indigo",
        color: "yellow",
    },
});

export default function MedicalPlansTable() {
    const classes = useStyles();
    const [rows, setRows] = useState<MedicalPlan[]>([]);

    useEffect(() => {
        axios.post("http://localhost:8080/medicalPlans",  {sessionId: localStorage.getItem("sessionId")}).then(response => {
            debugger
            console.log(response.data);
            setRows(response.data);
        });
    }, []);

    return (
        <Paper className={classes.root}>
            <Table className={classes.table} stickyHeader aria-label="sticky table">
                <TableHead>
                    <TableRow>
                        <TableCell align="left">Meds</TableCell>
                        <TableCell align="right">Start Date</TableCell>
                        <TableCell align="right">Stop Date</TableCell>
                        <TableCell align="right">Intake interval</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map(row => (
                        <TableRow key={row.medicalPlanId}>
                            <TableCell align="left">{row.meds}</TableCell>
                            <TableCell align="right">{row.startMed}</TableCell>
                            <TableCell align="right">{row.stopMed}</TableCell>
                            <TableCell align="right">{row.timePeriod}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </Paper>
    );
}