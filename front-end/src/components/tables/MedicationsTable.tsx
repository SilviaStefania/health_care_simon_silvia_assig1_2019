import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import axios from "axios";
import Medication from "../../types/Medication";

const useStyles = makeStyles({
    root: {
        width: '100%',
        overflowX: 'auto',
    },
    table: {
        minWidth: 650,
    },
    button: {
        margin: "20px 20px",
        backgroundColor: "indigo",
        color: "yellow",
    },
});

export default function MedicationsTable() {
    const classes = useStyles();
    const [rows, setRows] = useState<Medication[]>([]);

    useEffect(()=> {
        axios.get("http://localhost:8080/medications").then(response => {
            console.log(response.data);
            setRows(response.data);
        });
    },[]);

    return (
        <Paper className={classes.root}>
            <Table className={classes.table} stickyHeader aria-label="sticky table">
                <TableHead>
                    <TableRow>
                        <TableCell>Medication ID</TableCell>
                        <TableCell align="right">Medication Name</TableCell>
                        <TableCell align="right">Dosage</TableCell>
                        <TableCell align="right">Side Effects</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map(row => (
                        <TableRow key={row.medId}>
                            <TableCell  component="th" scope="row">
                                {row.medId}
                            </TableCell>
                            <TableCell align="right">{row.medName}</TableCell>
                            <TableCell align="right">{row.dosage}</TableCell>
                            <TableCell align="right">{row.sideEffects}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </Paper>
    );
}